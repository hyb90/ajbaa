import 'package:app/Extension/ColorExtension.dart';
import 'package:app/models/tree_photo.dart';
import 'package:app/pages/addImage.dart';
import 'package:app/pages/addNew.dart';
import 'package:app/pages/homePage.dart';
import 'package:app/services/api.dart';
import 'package:app/widgets/network_widget.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class treeHome extends StatefulWidget {
  @override
  _treeHome createState() => _treeHome();
}

class _treeHome extends State<treeHome> {
  String selected = '0';
  bool _value = false;
  bool _valuet = false;

  final _texto = TextEditingController();
  final _textt = TextEditingController();
  final _textth = TextEditingController();
  final _textf = TextEditingController();
  final _textfi = TextEditingController();
  final _texts = TextEditingController();
  final _textn = TextEditingController();
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  String problem="";
  Flushbar flush;
  List<TreePhoto> photos=[];
  getPhotos()async{
    setState(() {
      problem="";
      loading=true;
    });
    try{
      final response = await _provider.get('familyPhotos');
      if(response.toString()=="{message: Unauthenticated.}") {
        setState(() {
          problem=response["message"].toString();
          loading=false;
          print(problem);
        });

      }else {
        setState(() {
          for (Map i in response) {
            TreePhoto p = TreePhoto.fromJson(i);
            photos.add(p);
            photos = photos.reversed.toList();
          }
          loading = false;
        });
      }
    }catch(e){
      setState(() {
        problem=e.toString();
        loading=false;
      });
    }
  }
  @override
  void initState() {
    super.initState();
    getPhotos();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(body: Stack(

        children: <Widget>[Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.fill)),
            child:
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: HexColor.fromHex("#271F0D").withOpacity(0.47),
              child:

              Stack(
                children: <Widget>[

                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          color: HexColor.fromHex("#C2B19D"),
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(
                              color: HexColor.fromHex("#707070"))),
                      child: Column(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      0, 10, 0, 5),
                                  child: Text(
                                    'شجرة العائلة',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 22),
                                  ),
                                ),
                              ),
                            ],

                          ),
                          Flexible(
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              child: Padding(
                                  padding:
                                  EdgeInsets.fromLTRB(15, 15, 15, 10),
                                  child: loading?Center(child: Loading(loadingMessage: 'جاري تحميل الصور',)):problem!=""?Center(child: Error(errorMessage: problem,onRetryPressed: getPhotos,)):photos.isEmpty?Center(child:Text('لا يوجد صور حتى الآن')):GridView.builder(
                                    itemCount: photos.length,
                                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
                                    itemBuilder: (context,index)=>Container(
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: photos[index].familyPhotoURL==null?AssetImage(
                                                  'assets/images/msgsend.png'):NetworkImage(photos[index].familyPhotoURL),
                                              fit: BoxFit.cover
                                          ),
                                          color: Colors.white,
                                          border: Border.all(
                                              color:
                                              HexColor.fromHex(
                                                  "#C2B19D"))),
                                    ),)
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ) )],),appBar:  AppBar(
        //  centerTitle: true,  automaticallyImplyLeading: false,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.all(5),
            child: Image.asset(
              'assets/images/logo.png',
              width: 85,
              height: 70,
            ),
          ),
        ],
        title:  GestureDetector(child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(6),
              decoration: BoxDecoration(
                color: HexColor.fromHex("#48341A").withOpacity(0.4),
                borderRadius: BorderRadius.circular(25),
              ),
              child: Icon(Icons.home),
            )
          ],
        ),onTap: (){
          Route route = MaterialPageRoute(builder: (context) => HomeScreen());
          Navigator.pushReplacement(context, route);
        },),
        backgroundColor: HexColor.fromHex("#67523A").withOpacity(1),
      ),);
  }
}
