import 'package:app/Extension/ColorExtension.dart';
import 'package:app/pages/addImage.dart';
import 'package:app/pages/addNew.dart';
import 'package:app/pages/homePage.dart';
import 'package:app/pages/newDetails.dart';
import 'package:app/widgets/network_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/services/api.dart';
import 'dart:async';
import 'package:flushbar/flushbar.dart';
import 'package:app/models/new.dart';
class newHome extends StatefulWidget {
  @override
  _newHome createState() => _newHome();
}

class _newHome extends State<newHome> {
  String selected = '0';
  bool _value = false;
  bool _valuet = false;
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  String problem="";
  Flushbar flush;
  List<New> news=[];
  getNews()async{

    setState(() {
      problem="";
      loading=true;
    });
    try{
      final response = await _provider.get('news');
      if(response.toString()=="{message: Unauthenticated.}") {
        setState(() {
          problem=response["message"].toString();
          loading=false;
          print(problem);
        });

      }else {
        setState(() {
          for (Map i in response) {
            New n = New.fromJson(i);
            news.add(n);
            news = news.reversed.toList();
          }
          loading = false;
        });
      }
    }catch(e){
      setState(() {
        problem=e.toString();
        loading=false;
      });
    }
  }
  final _texto = TextEditingController();
  final _textt = TextEditingController();
  final _textth = TextEditingController();
  final _textf = TextEditingController();
  final _textfi = TextEditingController();
  final _texts = TextEditingController();
  final _textn = TextEditingController();

  @override
  void initState() {
    super.initState();
    getNews();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return
      Scaffold(
        appBar: AppBar(
          //  centerTitle: true,  automaticallyImplyLeading: false,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.all(5),
              child: Image.asset(
                'assets/images/logo.png',
                width: 85,
                height: 70,
              ),
            ),
          ],
          title: GestureDetector(child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(6),
                decoration: BoxDecoration(
                  color: HexColor.fromHex("#48341A").withOpacity(0.4),
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Icon(Icons.home),
              )
            ],
          ),onTap: (){
            Route route = MaterialPageRoute(builder: (context) => HomeScreen());
            Navigator.pushReplacement(context, route);
          },),
          backgroundColor: HexColor.fromHex("#67523A").withOpacity(1),
        ) ,body:  Stack(

        children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/background.png'),
                      fit: BoxFit.fill)),
              child:
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: HexColor.fromHex("#271F0D").withOpacity(0.47),
                child:
                Stack(
                  children: <Widget>[

                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        decoration: BoxDecoration(
                            color: HexColor.fromHex("#C2B19D"),
                            borderRadius: BorderRadius.circular(25),
                            border: Border.all(
                                color: HexColor.fromHex("#707070"))),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                              child: Text(
                                'أخبارنا',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 22),
                              ),
                            ),
                            GestureDetector(
                              child: Padding(
                                padding:
                                EdgeInsets.fromLTRB(30, 10, 20, 10),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              30, 10, 30, 10),
                                          child: Text(
                                            'كتابة خبر',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                                fontSize: 16),
                                          ),
                                        ),
                                        // width: MediaQuery.of(context).size.width,
                                        //  height: MediaQuery.of(context).size.height,
                                        decoration: BoxDecoration(
                                            color:
                                            HexColor.fromHex("#342109"),
                                            borderRadius:
                                            BorderRadius.circular(15),
                                            border: Border.all(
                                                color: HexColor.fromHex(
                                                    "#C2B19D"))))
                                  ],
                                ),
                              ),
                              onTap: () {
                                Navigator.of(context).pushReplacement(
                                  PageRouteBuilder(
                                    pageBuilder: (_, __, ___) => addNew(),
                                  ),
                                );
                              },
                            ),
                            Flexible(
                              child: loading?Center(child: Loading(loadingMessage: 'جاري تحميل الأخبار',)):problem!=""?Center(child: Error(errorMessage: problem,onRetryPressed: getNews,)):news.isEmpty?Center(child:Text('لا يوجد أخبار حتى الآن')):ListView.builder(
                                itemCount:news.length ,
                                itemBuilder:(BuildContext context, int index)=>GestureDetector(child: Padding(
                                  padding: EdgeInsets.fromLTRB(30, 10, 20, 10),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          new Spacer(),
                                          Text(
                                            news[index].title,
                                            textAlign: TextAlign.left,
                                          )
                                        ],
                                      ),
                                      Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(0, 5, 15, 5),
                                        child: Row(
                                          children: <Widget>[
                                            new Spacer(),
                                            Text(
                                             news[index].author,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(0, 5, 0, 5),
                                        child: Divider(
                                          color: Colors.black,
                                          height: 1,
                                        ),
                                      )
                                    ],
                                  ),
                                ),onTap: (){
                                  Navigator.of(context).push(
                                    PageRouteBuilder(
                                      pageBuilder: (_, __, ___) => newDetails(news[index]),
                                    ),
                                  );
                                },),

                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ) )],),);
  }
}
