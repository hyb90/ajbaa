import 'package:app/Extension/ColorExtension.dart';
import 'package:app/pages/addImage.dart';
import 'package:app/pages/addNew.dart';
import 'package:app/pages/homePage.dart';
import 'package:app/pages/newDetails.dart';
import 'package:app/services/api.dart';
import 'package:app/widgets/network_widget.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/models/message.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
class messageDetails extends StatefulWidget {
  final int senderId;
  final String Sender;
  messageDetails(this.senderId,this.Sender);
  @override
  _messageDetails createState() => _messageDetails();
}

class _messageDetails extends State<messageDetails> {
  String selected = '0';
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController _scrollController = new ScrollController();
  TextEditingController _msgText = TextEditingController();
  String msgText="";
  List<Message> msgs=[];
  int loggedId;
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  String problem="";
  Flushbar flush;
  bool loadingSend=false;
  getMsg()async{
    setState(() {
      msgs=[];
      problem="";
      loading=true;
    });
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user =jsonDecode(localStorage.getString('user'));
    setState(() {
      loggedId=user["id"];
    });
    var data={
    "reciver_id":loggedId.toString(),
    "sender_id":widget.senderId.toString()
    };
    print(data);
    try{
      final response = await _provider.post('MessageGet', data);
      if(response.toString()=="{message: Unauthenticated.}") {
        setState(() {
          problem=response["message"].toString();
          loading=false;
          print(problem);
        });

      }else {
        setState(() {
          for (Map i in response) {
            Message m = Message.fromJson(i);
            msgs.add(m);
          }
          msgs.sort((a, b) => a.createdAt.compareTo(b.createdAt));
          msgs=msgs.reversed.toList();
          loading = false;
        });
      }
    }catch(e){
      setState(() {
        problem=e.toString();
        loading=false;
      });
    }
  }
  sendMsg()async{
    var data={
      "sender_id":loggedId.toString(),
      "reciver_id":widget.senderId.toString(),
      "message":msgText
    };
    print(data);
    if(msgText=="") {
      flush = Flushbar(
          title: 'عذرا"',
          message: 'يرجى كتابة نص الرسالة',
          duration: Duration(seconds: 2),
          margin: EdgeInsets.all(20),
          borderRadius: 10,
          borderColor: Colors.black.withOpacity(0.5),
          backgroundColor: Colors.black.withOpacity(0.1),
          mainButton: IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              flush.dismiss(true);
            },
            color: Colors.white,
          ))
        ..show(context);
    }else
    {
      setState(() {
        problem="";
        loadingSend=true;
      });
      {
        final response = await _provider.post('MessageSend', data);
        if(response=="Message Sent") {
          setState(() {
            _msgText.text="";
            loadingSend = false;
          });
          getMsg();
        }else{
          setState(() {
            loadingSend = false;
          });
          //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>newHome()));
          flush = Flushbar(
              title: 'عذرا"',
              message: 'حدث خطأ ما يرجى المحاولة لاحقا"',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              borderColor: Colors.black.withOpacity(0.5),
              backgroundColor: Colors.black.withOpacity(0.1),
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
        }
      }
    }
  }
  @override
  void initState() {
    super.initState();
    getMsg();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //  extendBodyBehindAppBar: true,
        key: _scaffoldKey,
        //  backgroundColor: HexColor.fromHex("#67523A").withOpacity(1),

        appBar: AppBar(
          //  centerTitle: true,  automaticallyImplyLeading: false,
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.all(5),
              child: Image.asset(
                'assets/images/logo.png',
                width: 85,
                height: 70,
              ),
            ),
          ],
          title: GestureDetector(
            child: Row(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    color: HexColor.fromHex("#48341A").withOpacity(0.4),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Icon(Icons.home),
                )
              ],
            ),
            onTap: () {
              Route route =
                  MaterialPageRoute(builder: (context) => HomeScreen());
              Navigator.pushReplacement(context, route);
            },
          ),
          backgroundColor: HexColor.fromHex("#67523A").withOpacity(1),
        ),
        body: Stack(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/background.png'),
                        fit: BoxFit.fill)),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: HexColor.fromHex("#271F0D").withOpacity(0.47),
                  child: Stack(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                              color: HexColor.fromHex("#C2B19D"),
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(
                                  color: HexColor.fromHex("#707070"))),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                                child: Text(
                                  widget.Sender,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 22),
                                ),
                              ),
                              StreamBuilder(
                                stream: Stream.periodic(Duration(seconds: 15))
                                    .asyncMap((i) => getMsg()), // i is null here (check periodic docs)
                                builder: (context, snapshot) => Flexible(
                                  child: loading?Center(child: Loading(loadingMessage: 'جاري تحديث الرسائل',)):problem!=""?Center(child: Error(errorMessage: problem,onRetryPressed: getMsg,)):msgs.isEmpty?Center(child:Text('لا يوجد رسائل حتى الآن')):ListView.builder(
                                    //controller: _scrollController,
                                      itemCount: msgs.length,
                                      reverse: true,
                                      itemBuilder: (context,index)=>
                                      msgs[index].reciverId==loggedId?
                                      Padding(padding: EdgeInsets.fromLTRB(40,10, 10, 10),child: Container(
                                          width: 326,
                                          height: 88,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/msgsend.png'),
                                                  fit: BoxFit.fill)),
                                          child: Padding(padding: EdgeInsets.all(20),child:
                                          Text(msgs[index].message,textAlign: TextAlign.end,),)),):
                                      Padding(child: Container(
                                          width: 326,
                                          height: 88,
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/msgRe.png'),
                                                  fit: BoxFit.fill)),
                                          child: Padding(padding: EdgeInsets.all(20),child:
                                          Text(msgs[index].message,textAlign: TextAlign.start,style: TextStyle(color:
                                          Colors.white),),)),padding: EdgeInsets.fromLTRB(10,10, 40, 10),)),
                                ),
                              ),
                              Padding(
                                child: Align(
                                  child: Padding(
                                    child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(20),
                                            border: Border.all(
                                                color: HexColor.fromHex("#707070"),
                                                width: 1)),
                                        child: Row(
                                          children: [
                                            Padding(
                                              child: !loadingSend?GestureDetector(
                                                onTap: sendMsg,
                                                child:Image.asset(
                                                    'assets/images/sendicon.png'),
                                              ):CircularProgressIndicator(),
                                              padding:
                                              EdgeInsets.fromLTRB(10, 20, 10, 20),
                                            ),
                                            Flexible(
                                                child: TextField(
                                                  onChanged: (value){
                                                    setState(() {
                                                      msgText=value;
                                                    });
                                                  },
                                                  textAlign: TextAlign.right,
                                                  controller: _msgText,
                                                  cursorColor: Colors.transparent,
                                                  style: TextStyle(color: Colors.black),
                                                  decoration: InputDecoration(
                                                    hintText: 'الرسالة',

                                                    hintStyle: TextStyle(
                                                      color: HexColor.fromHex("#342109")
                                                          .withOpacity(0.25),
                                                    ),
                                                    filled: true,
                                                    labelStyle: Theme.of(context)
                                                        .textTheme
                                                        .caption
                                                        .copyWith(
                                                        color:
                                                        Theme.of(context).primaryColor),

                                                    fillColor: Colors.transparent,

                                                    //can also add icon to the end of the textfiled
                                                    //  suffixIcon: Icon(Icons.remove_red_eye),
                                                  ),
                                                ),
                                            ),

                                          ],
                                        )),
                                    padding: EdgeInsets.all(20),
                                  ),
                                  alignment: Alignment.bottomCenter,
                                ),
                                padding: EdgeInsets.all(10),
                              )
                            ],
                          ),
                        ),
                      ),

                    ],
                  ),
                ))
          ],
        ));
  }
}
