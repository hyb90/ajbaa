import 'package:app/Extension/ColorExtension.dart';
import 'package:app/pages/addImage.dart';
import 'package:app/pages/addNew.dart';
import 'package:app/pages/messageDetails.dart';
import 'package:app/widgets/network_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:app/services/api.dart';
import 'package:flushbar/flushbar.dart';
import 'package:app/models/conversation.dart';
class messageActivity extends StatefulWidget {
  @override
  _messageActivity createState() => _messageActivity();
}

class _messageActivity extends State<messageActivity> {
  String selected = '0';
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  String problem="";
  Flushbar flush;
  List<Conversation> convs=[];
  getConver()async{
    setState(() {
      problem="";
      loading=true;
    });
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user =jsonDecode(localStorage.getString('user'));
    String receiver=user["id"].toString();
    var data={
      "reciver_id":receiver,
    };
    print(data);
    try{
      final response = await _provider.post('MessageGetLastAll', data);
      if(response.toString()=="{message: Unauthenticated.}") {
        setState(() {
          problem=response["message"].toString();
          loading=false;
          print(problem);
        });

      }else {
        setState(() {
          for (Map i in response) {
            Conversation c = Conversation.fromJson(i);
            convs.add(c);
            convs = convs.reversed.toList();
          }
          loading = false;
        });
      }
    }catch(e){
      setState(() {
        problem=e.toString();
        loading=false;
      });
    }
  }
  @override
  void initState() {
    super.initState();
    getConver();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.fill)),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: HexColor.fromHex("#271F0D").withOpacity(0.47),
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          color: HexColor.fromHex("#C2B19D"),
                          borderRadius: BorderRadius.circular(25),
                          border:
                              Border.all(color: HexColor.fromHex("#707070"))),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                            child: Text(
                              'رسائل',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 22),
                            ),
                          ),
                          Flexible(
                            child:loading?Center(child: Loading(loadingMessage: 'جاري تحميل الرسائل',)):problem!=""?Center(child: Error(errorMessage: problem,onRetryPressed: getConver,)):convs.isEmpty?Center(child:Text('لا يوجد رسائل حتى الآن')):ListView.builder(
                              itemCount: convs.length,
                              itemBuilder: (context,index)=>GestureDetector(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(30, 10, 20, 10),
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          new Spacer(),
                                          Text(
                                            convs[index].sender,
                                            textAlign: TextAlign.left,
                                          )
                                        ],
                                      ),
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(0, 5, 15, 5),
                                        child: Row(
                                          children: <Widget>[
                                            new Spacer(),
                                            Text(
                                              convs[index].message,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.white),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                        child: Divider(
                                          color: Colors.black,
                                          height: 1,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  Navigator.of(context).push(
                                    PageRouteBuilder(
                                      pageBuilder: (_, __, ___) => messageDetails(convs[index].senderId,convs[index].sender),
                                    ),
                                  );
                                },
                              ),

                            )

                          ),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ))
      ],
    );
  }
}
