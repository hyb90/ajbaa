import 'dart:convert';

import 'package:app/Extension/ColorExtension.dart';
import 'package:app/pages/addImage.dart';
import 'package:app/pages/addNew.dart';
import 'package:app/pages/homePage.dart';
import 'package:app/pages/messageDetails.dart';
import 'package:app/services/api.dart';
import 'package:app/widgets/network_widget.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
class contactHome extends StatefulWidget {
  @override
  _contactHome createState() => _contactHome();
}

class _contactHome extends State<contactHome> {
  String selected = '0';
  bool _value = false;
  bool _valuet = false;
  List<User> users=[];
  final _texto = TextEditingController();
  final _textt = TextEditingController();
  final _textth = TextEditingController();
  final _textf = TextEditingController();
  final _textfi = TextEditingController();
  final _texts = TextEditingController();
  final _textn = TextEditingController();
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  String problem="";
  Flushbar flush;
  getUsers()async{
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user =jsonDecode(localStorage.getString('user'));
    print(user);
    String loggedUser=user["name"];
    print(loggedUser);
    setState(() {
      problem="";
      loading=true;
    });
    try{
      final response = await _provider.get('UserPhotos');
      if(response.toString()=="{message: Unauthenticated.}") {
        setState(() {
          problem=response["message"].toString();
          loading=false;
          print(problem);
        });

      }else {
        setState(() {
          for (Map i in response) {
            User u = User.fromJson(i);
            print(u.name);
            if(u.name!=loggedUser)
            users.add(u);
          }
          loading = false;
        });
      }
    }catch(e){
      setState(() {
        problem=e.toString();
        loading=false;
      });
    }
  }


  @override
  void initState() {
    super.initState();
    getUsers();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(appBar: AppBar(
        //  centerTitle: true,  automaticallyImplyLeading: false,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.all(5),
            child: Image.asset(
              'assets/images/logo.png',
              width: 85,
              height: 70,
            ),
          ),
        ],
        title: GestureDetector(child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(6),
              decoration: BoxDecoration(
                color: HexColor.fromHex("#48341A").withOpacity(0.4),
                borderRadius: BorderRadius.circular(25),
              ),
              child: Icon(Icons.home),
            )
          ],
        ),onTap: (){
          Route route = MaterialPageRoute(builder: (context) => HomeScreen());
          Navigator.pushReplacement(context, route);
        },),
        backgroundColor: HexColor.fromHex("#67523A").withOpacity(1),
      ) ,
        body: Stack(

        children: <Widget>[Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.fill)),
            child:
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: HexColor.fromHex("#271F0D").withOpacity(0.47),
              child:
              Stack(
                children: <Widget>[

                  Padding(

                    padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          color: HexColor.fromHex("#C2B19D"),
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(
                              color: HexColor.fromHex("#707070"))),
                      child:  loading?Center(child: Loading(loadingMessage: 'جاري تحميل البيانات',)):problem!=""?Center(child: Error(errorMessage: problem,onRetryPressed: getUsers,)):users.isEmpty?Center(child:Text('لا يوجد معلومات للعرض')):Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                            child: Text(
                              'تواصلنا',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 22),
                            ),
                          ),
                         Flexible(
                            child: GridView.builder(
                                itemCount: users.length,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                                itemBuilder: (context,index)=>GestureDetector(
                                  onTap:(){
                                    Dialog contactDialog = Dialog(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)), //this right here
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: HexColor.fromHex("#C2B19D"),
                                            borderRadius: BorderRadius.circular(25),
                                            border: Border.all(
                                                color: HexColor.fromHex("#707070"))),
                                        height: 300.0,
                                        width: 300.0,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding:  EdgeInsets.all(15.0),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Icon(Icons.person_outline_rounded,color:Colors.black54 ),
                                                  Text(users[index].name, style: TextStyle(color: Colors.black54),),


                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.all(15.0),
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Icon(Icons.phone_android,color:Colors.black54 ),
                                                  Text(users[index].phoneNumber, style: TextStyle(color: Colors.black54),),

                                                ],
                                              ),
                                            ),
                                            Padding(padding: EdgeInsets.only(top: 20.0)),
                                            FlatButton(
                                                onPressed: (){Navigator.of(context).push(
                                                  PageRouteBuilder(
                                                    pageBuilder: (_, __, ___) => messageDetails(users[index].id,users[index].name),
                                                  ),
                                                );},
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Icon(Icons.message_rounded,color:Colors.black54 ,),
                                                    Text('كتابة رسالة', style: TextStyle(color: Colors.black54, fontSize: 18.0),),


                                                  ],
                                                ))
                                          ],
                                        ),
                                      ),
                                    );
                                    showDialog(context: context, builder: (BuildContext context) => contactDialog);},
                                  child: Padding(padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                    child:   Container(

                                      // width: MediaQuery.of(context).size.width,
                                      child: Padding(
                                        padding:
                                        EdgeInsets.fromLTRB(5, 15, 5, 10),
                                        child: Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                5, 5, 5, 5),
                                            child: Container(
                                                decoration: BoxDecoration(
                                                    image:DecorationImage(
                                                      image:  users[index].gender=="ذكر"?NetworkImage(users[index].userPhotoPath):AssetImage('assets/images/prot.png',) ,
                                                      fit: BoxFit.cover
                                                    ),
                                                    color: Colors.transparent,borderRadius: BorderRadius.circular(20),
                                                    border: Border.all(
                                                        color:
                                                        HexColor.fromHex(
                                                            "#342109"))),
                                                height: 89,
                                                width: 89,

                                      ),
                                    ),),
                                )),
                          )


                            )
                         )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ) )],),);
  }
}
