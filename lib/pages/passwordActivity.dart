import 'package:app/Extension/ColorExtension.dart';
import 'package:app/pages/homePage.dart';
import 'package:app/widgets/network_widget.dart';
import 'package:countup/countup.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:app/services/api.dart';
import 'dart:async';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
class passwordA extends StatefulWidget {
  final String nationalId;
  passwordA(this.nationalId);
  @override
  _passwordA createState() => new _passwordA();
}

class _passwordA extends State<passwordA> with SingleTickerProviderStateMixin {
  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textThirdFocusNode = new FocusNode();
  FocusNode textFourthFocusNode = new FocusNode();
  final _num = TextEditingController();
  final _numt = TextEditingController();
  final _numth = TextEditingController();
  final _numf = TextEditingController();
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  String problem="";
  Flushbar flush;
  login()async{
    var data={
      "nationalId":widget.nationalId,
      "password":_num.text+_numt.text+_numth.text+_numf.text
    };
    setState(() {
      loading=true;
    });
    try{
      int r=0;
      final response = await _provider.login('login',data);
      print(response);
      if(response.toString().length==43) {
        SharedPreferences localStorage = await SharedPreferences.getInstance();
        localStorage.setString('token', json.encode(response.toString()));
        r=await getUser();
        if(r==1) {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => HomeScreen()),
              (route) => false);
          flush = Flushbar(
              title: 'أهلا وسهلا',
              message: 'أهلا وسهلا بك في تطبيق العائلة',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              borderColor: Colors.black.withOpacity(0.5),
              backgroundColor: Colors.black.withOpacity(0.1),
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
        }else{
          flush = Flushbar(
              title: 'عذرا هناك خطأ ما',
              message: 'يرجى المحاولة مرة أخرى',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              borderColor: Colors.black.withOpacity(0.5),
              backgroundColor: Colors.black.withOpacity(0.1),
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
        }
      }else if(response["error"]!=null){
        flush = Flushbar(
            title: 'عذرا هناك خطأ ما',
            message: 'هناك مشكلة بالرقم الوطني أو كلمة المرور',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.black.withOpacity(0.5),
            backgroundColor: Colors.black.withOpacity(0.1),
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }else if(response["nationalId"]!=null){
        flush = Flushbar(
            title: 'عذرا هناك خطأ ما',
            message: 'هناك مشكلة بالرقم الوطني أو كلمة المرور',
            duration: Duration(seconds: 2),
            margin: EdgeInsets.all(20),
            borderRadius: 10,
            borderColor: Colors.black.withOpacity(0.5),
            backgroundColor: Colors.black.withOpacity(0.1),
            mainButton: IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                flush.dismiss(true);
              },
              color: Colors.white,
            ))
          ..show(context);
      }
      setState(() {
        loading=false;
      });
    }catch(e){
      setState(() {
        problem=e.toString();
        loading=false;
      });
    }
  }
  getUser()async{
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    try{
      final response = await _provider.get('GetUser');
      if(response["id"]!=null){
        localStorage.setString('user', json.encode(response));
        return 1;
      }
      else{
        return 0;
      }
    }catch(e){
        return 0;
    }
  }
  @override
  void initState() {

  }

  @override
  Widget build(BuildContext contextt) {
    return new Scaffold(
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.fill)),
            child: Stack(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    color: HexColor.fromHex("#271F0D").withOpacity(0.47),
                    child: loading?Center(child: Loading(loadingMessage: 'جاري تسجيل الدخول',)):problem!=""?Center(child: Error(errorMessage: problem,onRetryPressed: (){},)):Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 50),
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Align(
                                        alignment: Alignment.topCenter,
                                        child: Column(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  10, 50, 10, 40),
                                              child: Container(
                                                width: 349,
                                                height: 6,
                                                color:
                                                    HexColor.fromHex("#C2B19D")
                                                        .withOpacity(1),
                                              ),
                                            ),
                                          ],
                                        )),
                                    Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                new Spacer(),
                                                Column(
                                                  children: <Widget>[
                                                    Container(
                                                      width: 176,
                                                      height: 141,
                                                      decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                            image: AssetImage(
                                                              'assets/images/logo.png',
                                                            ),
                                                            fit: BoxFit.fill),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  15, 10, 15, 0),
                                              child: Center(
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      color: HexColor.fromHex(
                                                              "#48341A")
                                                          .withOpacity(0.4),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              25),
                                                      border: Border.all(
                                                          color:
                                                              HexColor.fromHex(
                                                                      "#707070")
                                                                  .withOpacity(
                                                                      0.4))),
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            30, 30, 30, 30),
                                                    child: Container(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Text(
                                                            'فضلا ادخل كلمة المرور',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                fontSize: 17),
                                                            textAlign: TextAlign
                                                                .center,
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets
                                                                .fromLTRB(10,
                                                                    30, 10, 30),
                                                            child: Container(
                                                              width:
                                                                  MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width,
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: <
                                                                    Widget>[
                                                                  Expanded(
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              5),
                                                                      child: Container(
                                                                          // width: 68,
                                                                          height: 50,
                                                                          child: Center(
                                                                            child: Theme(
                                                                                data: new ThemeData(
                                                                                    primaryColor: Colors.transparent,
                                                                                    // accentColor: Colors.orange,
                                                                                    hintColor: Colors.transparent),
                                                                                child: TextField(
                                                                                  onChanged: (value){
                                                                                    setState(() {
                                                                                      _num.text=value;
                                                                                      FocusScope.of(context).requestFocus(textSecondFocusNode);
                                                                                    });
                                                                                  },
                                                                                  textAlign: TextAlign.center,
                                                                                  controller: _num,
                                                                                  inputFormatters:[FilteringTextInputFormatter.digitsOnly,LengthLimitingTextInputFormatter(1)],
                                                                                  keyboardType: TextInputType.number,
                                                                                  maxLength: 1,
                                                                                  cursorColor: Colors.transparent,
                                                                                  style: TextStyle(color: Colors.black),
                                                                                  decoration: InputDecoration(
                                                                                    contentPadding: EdgeInsets.only(bottom: 0.0),

                                                                                    filled: true,
                                                                                    labelStyle: Theme.of(context).textTheme.caption.copyWith(color: Theme.of(context).primaryColor),

                                                                                    fillColor: Colors.transparent,
                                                                                    //can also add icon to the end of the textfiled
                                                                                    //  suffixIcon: Icon(Icons.remove_red_eye),
                                                                                  ),
                                                                                )),
                                                                          ),
                                                                          decoration: BoxDecoration(
                                                                            color:
                                                                                HexColor.fromHex("#C2B19D").withOpacity(1),
                                                                            borderRadius:
                                                                                BorderRadius.circular(15),
                                                                          )),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              5),
                                                                      child: Container(
                                                                          // width: 68,
                                                                          height: 50,
                                                                          child: Padding(
                                                                            padding: EdgeInsets.fromLTRB(
                                                                                0,
                                                                                0,
                                                                                0,
                                                                                0),
                                                                            child:
                                                                                Center(
                                                                              child: Theme(
                                                                                  data: new ThemeData(
                                                                                      primaryColor: Colors.transparent,
                                                                                      //   accentColor: Colors.orange,
                                                                                      hintColor: Colors.transparent),
                                                                                  child: TextField(
                                                                                    onChanged: (value){
                                                                                      setState(() {
                                                                                        _numt.text=value;
                                                                                        FocusScope.of(context).requestFocus(textThirdFocusNode);
                                                                                      });
                                                                                    },
                                                                                    focusNode: textSecondFocusNode,
                                                                                    textAlign: TextAlign.center,
                                                                                    controller: _numt,
                                                                                    inputFormatters:[FilteringTextInputFormatter.digitsOnly,LengthLimitingTextInputFormatter(1)],
                                                                                    keyboardType: TextInputType.number,
                                                                                    maxLength: 1,
                                                                                    cursorColor: Colors.transparent,
                                                                                    style: TextStyle(color: Colors.black),
                                                                                    decoration: InputDecoration(
                                                                                      filled: true,
                                                                                      contentPadding: EdgeInsets.only(bottom: 0.0),

                                                                                      labelStyle: Theme.of(context).textTheme.caption.copyWith(color: Theme.of(context).primaryColor),

                                                                                      fillColor: Colors.transparent,

                                                                                      //can also add icon to the end of the textfiled
                                                                                      //  suffixIcon: Icon(Icons.remove_red_eye),
                                                                                    ),
                                                                                  )),
                                                                            ),
                                                                          ),
                                                                          decoration: BoxDecoration(
                                                                            color:
                                                                                HexColor.fromHex("#C2B19D").withOpacity(1),
                                                                            borderRadius:
                                                                                BorderRadius.circular(15),
                                                                          )),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              5),
                                                                      child: Container(
                                                                          // width: 68,
                                                                          height: 50,
                                                                          child: Padding(
                                                                            padding: EdgeInsets.fromLTRB(
                                                                                0,
                                                                                0,
                                                                                0,
                                                                                0),
                                                                            child:
                                                                                Center(
                                                                              child: Theme(
                                                                                  data: new ThemeData(
                                                                                      primaryColor: Colors.transparent,
                                                                                      //   accentColor: Colors.orange,
                                                                                      hintColor: Colors.transparent),
                                                                                  child: TextField(
                                                                                    onChanged: (value){
                                                                                      setState(() {
                                                                                        _numth.text=value;
                                                                                        FocusScope.of(context).requestFocus(textFourthFocusNode);
                                                                                      });
                                                                                    },
                                                                                    focusNode: textThirdFocusNode,
                                                                                    textAlign: TextAlign.center,
                                                                                    controller: _numth,
                                                                                    inputFormatters:[FilteringTextInputFormatter.digitsOnly,LengthLimitingTextInputFormatter(1)],
                                                                                    keyboardType: TextInputType.number,
                                                                                    maxLength: 1,
                                                                                    cursorColor: Colors.transparent,
                                                                                    style: TextStyle(color: Colors.black),
                                                                                    decoration: InputDecoration(
                                                                                      filled: true,
                                                                                      contentPadding: EdgeInsets.only(bottom: 0.0),

                                                                                      labelStyle: Theme.of(context).textTheme.caption.copyWith(color: Theme.of(context).primaryColor),

                                                                                      fillColor: Colors.transparent,

                                                                                      //can also add icon to the end of the textfiled
                                                                                      //  suffixIcon: Icon(Icons.remove_red_eye),
                                                                                    ),
                                                                                  )),
                                                                            ),
                                                                          ),
                                                                          decoration: BoxDecoration(
                                                                            color:
                                                                                HexColor.fromHex("#C2B19D").withOpacity(1),
                                                                            borderRadius:
                                                                                BorderRadius.circular(15),
                                                                          )),
                                                                    ),
                                                                  ),
                                                                  Expanded(
                                                                    child:
                                                                        Padding(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              5),
                                                                      child: Container(
                                                                          // width: 68,
                                                                          height: 50,
                                                                          child: Padding(
                                                                            padding: EdgeInsets.fromLTRB(
                                                                                0,
                                                                                0,
                                                                                0,
                                                                                0),
                                                                            child:
                                                                                Center(
                                                                              child: Theme(
                                                                                  data: new ThemeData(
                                                                                      primaryColor: Colors.transparent,
                                                                                      //   accentColor: Colors.orange,
                                                                                      hintColor: Colors.transparent),
                                                                                  child: TextField(
                                                                                    onChanged: (value){
                                                                                      setState(() {
                                                                                        _numf.text=value;
                                                                                      });
                                                                                    },
                                                                                    focusNode: textFourthFocusNode,
                                                                                    textAlign: TextAlign.center,
                                                                                    controller: _numf,
                                                                                    maxLength: 1,
                                                                                    inputFormatters:[FilteringTextInputFormatter.digitsOnly,LengthLimitingTextInputFormatter(1)],
                                                                                    keyboardType: TextInputType.number,
                                                                                    cursorColor: Colors.transparent,
                                                                                    style: TextStyle(color: Colors.black),
                                                                                    decoration: InputDecoration(
                                                                                      filled: true,
                                                                                      contentPadding: EdgeInsets.only(bottom: 0.0),

                                                                                      labelStyle: Theme.of(context).textTheme.caption.copyWith(color: Theme.of(context).primaryColor),

                                                                                      fillColor: Colors.transparent,

                                                                                      //can also add icon to the end of the textfiled
                                                                                      //  suffixIcon: Icon(Icons.remove_red_eye),
                                                                                    ),
                                                                                  )),
                                                                            ),
                                                                          ),
                                                                          decoration: BoxDecoration(
                                                                            color:
                                                                                HexColor.fromHex("#C2B19D").withOpacity(1),
                                                                            borderRadius:
                                                                                BorderRadius.circular(15),
                                                                          )),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                          GestureDetector(
                                                            child: Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .fromLTRB(
                                                                          0,
                                                                          0,
                                                                          0,
                                                                          30),
                                                              child: Container(
                                                                  child:
                                                                      Padding(
                                                                    padding: EdgeInsets
                                                                        .fromLTRB(
                                                                            70,
                                                                            5,
                                                                            70,
                                                                            5),
                                                                    child: Text(
                                                                      'دخول',
                                                                      style: TextStyle(
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                  ),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: HexColor.fromHex(
                                                                            "#342109")
                                                                        .withOpacity(
                                                                            1),
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            10),
                                                                  )),
                                                            ),
                                                            onTap: login
                                                          ),
                                                        ],
                                                      ),
                                                      //height: 260,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            'الأجبع V 1:0',
                            style: GoogleFonts.stardosStencil(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.black),
                          ),
                        )
                      ],
                    )),
              ],
            )));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
