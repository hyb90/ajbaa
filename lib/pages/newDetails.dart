import 'dart:async';

import 'package:app/Extension/ColorExtension.dart';
import 'package:app/pages/homePage.dart';
import 'package:app/pages/newEdit.dart';
import 'package:app/pages/newHome.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/models/new.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:app/services/api.dart';
class newDetails extends StatefulWidget {
  final New n;
  newDetails(this.n);
  @override
  _newDetails createState() => new _newDetails();
}

class _newDetails extends State<newDetails> with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String userName;
  bool loading=false;
  String problem="";
  int id;
  ApiProvider _provider = ApiProvider();
  Flushbar flush;
  deleteNews()async{
    {
      var data={};
      setState(() {
        problem="";
        loading=true;
      });
      try {
        final response = await _provider.get('newsD/$id');

        if(response=="DeletedSuccess") {
          setState(() {
            loading = false;
          });
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>newHome()));
          flush = Flushbar(
              title: 'شكرا لك',
              message: 'تم حذف الخبر بنجاح',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              borderColor: Colors.black.withOpacity(0.5),
              backgroundColor: Colors.black.withOpacity(0.1),
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
        }else{
          setState(() {
            loading = false;
          });
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>newHome()));
          flush = Flushbar(
              title: 'عذرا"',
              message: 'حدث خطأ ما يرجى المحاولة لاحقا"',
              duration: Duration(seconds: 2),
              margin: EdgeInsets.all(20),
              borderRadius: 10,
              borderColor: Colors.black.withOpacity(0.5),
              backgroundColor: Colors.black.withOpacity(0.1),
              mainButton: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  flush.dismiss(true);
                },
                color: Colors.white,
              ))
            ..show(context);
        }
      } catch (e) {
        setState(() {
          print(e);
          problem = e.toString();
          loading = false;
        });
      }
    }
  }
  getUser()async{
  SharedPreferences localStorage = await SharedPreferences.getInstance();
  var user =jsonDecode(localStorage.getString('user'));
  setState(() {
    userName=user["name"];
  });

}
  @override
  void initState() {
    // navigationPage();
    super.initState();
    id=widget.n.newsId;
    getUser();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      //  extendBodyBehindAppBar: true,
      key: _scaffoldKey,
      //  backgroundColor: HexColor.fromHex("#67523A").withOpacity(1),

      appBar: AppBar(
        //  centerTitle: true,  automaticallyImplyLeading: false,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.all(5),
            child: Image.asset(
              'assets/images/logo.png',
              width: 85,
              height: 70,
            ),
          ),
        ],
        title:  GestureDetector(child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(6),
              decoration: BoxDecoration(
                color: HexColor.fromHex("#48341A").withOpacity(0.4),
                borderRadius: BorderRadius.circular(25),
              ),
              child: Icon(Icons.home),
            )
          ],
        ),onTap: (){
          Route route = MaterialPageRoute(builder: (context) => HomeScreen());
          Navigator.pushReplacement(context, route);
        },),
        backgroundColor: HexColor.fromHex("#67523A").withOpacity(1),
      ),

      body: SingleChildScrollView(child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.fill)),
        child: Stack(
          children: <Widget>[

            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: HexColor.fromHex("#271F0D").withOpacity(0.47),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Container(
                width: MediaQuery.of(context).size.width,
               // height:600,
                decoration: BoxDecoration(
                    color: HexColor.fromHex("#C2B19D"),
                    borderRadius: BorderRadius.circular(25),
                    border: Border.all(color: HexColor.fromHex("#707070"))),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                      child: Text(
                        widget.n.title,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        widget.n.author==userName?Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: IconButton(
                            icon: Icon(Icons.edit),
                            onPressed: (){Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>NewEdit(widget.n)));},
                          ),
                        ):Text(""),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Text(
                            widget.n.author,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 16,color: Colors.white),
                          ),
                        ),
                        widget.n.author==userName?Padding(
                          padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: IconButton(
                            icon: Icon(Icons.delete),
                            onPressed: deleteNews,
                          ),
                        ):Text(""),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: Divider(color: Colors.black,height: 2,),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: Align(child: Text(
                        widget.n.newsDetail,
                        textAlign: TextAlign.right,
                        style: TextStyle(fontSize: 16,color: Colors.black),
                      ),alignment: Alignment.bottomRight,),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),),
    );
  }
}
