import 'package:app/Extension/ColorExtension.dart';
import 'package:app/pages/addImage.dart';
import 'package:app/pages/addNew.dart';
import 'package:app/pages/homePage.dart';
import 'package:app/services/api.dart';
import 'package:app/widgets/network_widget.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app/models/photo.dart';
class imageActivity extends StatefulWidget {
  @override
  _imageActivity createState() => _imageActivity();
}

class _imageActivity extends State<imageActivity> {
  String selected = '0';
  bool _value = false;
  bool _valuet = false;

  final _texto = TextEditingController();
  final _textt = TextEditingController();
  final _textth = TextEditingController();
  final _textf = TextEditingController();
  final _textfi = TextEditingController();
  final _texts = TextEditingController();
  final _textn = TextEditingController();
  ApiProvider _provider = ApiProvider();
  bool loading=false;
  String problem="";
  Flushbar flush;
  List<Photo> photos=[];
  getPhotos()async{
    setState(() {
      problem="";
      loading=true;
    });
    try{
      final response = await _provider.get('photos');
      if(response.toString()=="{message: Unauthenticated.}") {
        setState(() {
          problem=response["message"].toString();
          loading=false;
          print(problem);
        });

      }else {
        setState(() {
          for (Map i in response) {
            Photo p = Photo.fromJson(i);
            photos.add(p);
            photos = photos.reversed.toList();
          }
          loading = false;
        });
      }
    }catch(e){
      setState(() {
        problem=e.toString();
        loading=false;
      });
    }
  }
  @override
  void initState() {
    super.initState();
    getPhotos();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      Stack(

        children: <Widget>[Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.fill)),
            child:
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              color: HexColor.fromHex("#271F0D").withOpacity(0.47),
              child:
              Stack(
                children: <Widget>[

                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          color: HexColor.fromHex("#C2B19D"),
                          borderRadius: BorderRadius.circular(25),
                          border: Border.all(
                              color: HexColor.fromHex("#707070"))),
                      child: Column(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      0, 10, 0, 5),
                                  child: Text(
                                    'صور وأفراح',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 22),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          GestureDetector(
                            child: Padding(
                              padding:
                              EdgeInsets.fromLTRB(10, 10, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(
                                            10, 10, 10, 10),
                                        child: Text(
                                          ' إضافة صورة',
                                          style: TextStyle(
                                              fontWeight:
                                              FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 16),
                                        ),
                                      ),
                                      // width: MediaQuery.of(context).size.width,
                                      //  height: MediaQuery.of(context).size.height,
                                      decoration: BoxDecoration(
                                          color: HexColor.fromHex(
                                              "#342109"),
                                          borderRadius:
                                          BorderRadius.circular(
                                              15),
                                          border: Border.all(
                                              color: HexColor.fromHex(
                                                  "#C2B19D"))))
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).push(
                                PageRouteBuilder(
                                  pageBuilder: (_, __, ___) =>
                                      addImage(),
                                ),
                              );
                            },
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: Padding(
                                padding:
                                EdgeInsets.fromLTRB(15, 15, 15, 10),
                                child: loading?Center(child: Loading(loadingMessage: 'جاري تحميل الصور',)):problem!=""?Center(child: Error(errorMessage: problem,onRetryPressed: getPhotos,)):photos.isEmpty?Center(child:Text('لا يوجد صور حتى الآن')):GridView.builder(
                                  itemCount: photos.length,
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                                  itemBuilder: (context,index)=>Container(
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: NetworkImage(photos[index].photoURL),
                                            fit: BoxFit.cover
                                        ),
                                        color: Colors.white,
                                        border: Border.all(
                                            color:
                                            HexColor.fromHex(
                                                "#C2B19D"))),
                                  ),)
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ) )],);
  }
}
