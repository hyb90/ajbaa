import 'custom_exception.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
class ApiProvider {

  final String _baseUrl = "https://public.alajb3.com/api/";

  var token;
  _setHeaders() => {
    'Content-type' : 'application/json',
    'Accept' : 'application/json',
    'Authorization': 'Bearer $token'
  };
  _setHeaders2() => {
    'Content-type' : 'multipart/form-data',
    'Authorization': 'Bearer $token'
  };
  _getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    token=jsonDecode(localStorage.getString('token'));
    print(token);
  }
  Future<dynamic> get(String url) async {
    await _getToken();
    var responseJson;
    var fullUrl = _baseUrl + url;
    try {
      final response = await http.get(
        fullUrl,
        headers: _setHeaders(),
      );
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }
  Future<dynamic> post(String url, var data) async {
    await _getToken();
    var responseJson;
    var fullUrl = _baseUrl + url;
    try {
      final response = await http.post(
        fullUrl,
        headers: _setHeaders(),
        body: jsonEncode(data)
      );
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }
  Future<dynamic> post2(String url, var data) async {
    await _getToken();
    var responseJson;
    var fullUrl = _baseUrl + url;
    try {
      final response = await http.post(
          fullUrl,
          headers: _setHeaders(),
          body: jsonEncode(data)
      );
      responseJson = _response2(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }
  Future<dynamic> login(String url, var data) async {
    var responseJson;
    var fullUrl = _baseUrl + url;
    try {
      final response = await http.post(
          fullUrl,
          headers: _setHeaders(),
          body: jsonEncode(data)
      );
      responseJson = _responseLogin(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  //fetch any exception
  dynamic _responseLogin(http.Response response) {
    switch (response.statusCode) {
      case 200:
          print(response.body);
          if (response.body.toString()[0] != '{') {
            return response.body;
          } else {
            var responseJson = json.decode(response.body.toString());
            return responseJson;
          }break;
      case 400:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return response;
      case 401:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 402:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 403:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 500:
        throw FetchDataException("Something went wrong. Please try again later.");
      default:
        throw FetchDataException(response.body.toString()+'error code : '+response.statusCode.toString());
    }
  }
  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        print(response.body);
          var responseJson = json.decode(response.body.toString());
          return responseJson;
      case 201:
        print(response.body);
        var responseJson = json.decode(response.body);
        return responseJson;
      case 400:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 401:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 402:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 403:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 500:
        throw FetchDataException("Something went wrong. Please try again later.");
      default:
        throw FetchDataException(response.body.toString()+'error code : '+response.statusCode.toString());
    }
  }
  dynamic _response2(http.Response response) {
    switch (response.statusCode) {
      case 200:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return response.body;
      case 201:
        print(response.body);
        var responseJson = json.decode(response.body);
        return responseJson;
      case 400:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 401:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 402:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 403:
        print(response.body);
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 500:
        throw FetchDataException("Something went wrong. Please try again later.");
      default:
        throw FetchDataException(response.body.toString()+'error code : '+response.statusCode.toString());
    }
  }

}