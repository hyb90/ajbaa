class TreePhoto {
  int familyPhotoId;
  String familyPhotoURL;


  TreePhoto(
      {this.familyPhotoId,
        this.familyPhotoURL,
      });

  TreePhoto.fromJson(Map<String, dynamic> json) {
    familyPhotoId = json['familyPhotoId'];
    familyPhotoURL = json['familyPhotoURL'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['familyPhotoId'] = this.familyPhotoId;
    data['familyPhotoURL'] = this.familyPhotoURL;
    return data;
  }
}

