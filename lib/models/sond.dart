class Sond {
  int numberShares;
  int balance;
  int arrears;
  String loan;
  String remainingLoan;
  String arrearsLoan;
  String incorporationFee;
  String filtering;
  String waitingLoan;
  String investor;
  int amountInvestment;
  String note;
  int userId;
  String userName;
  String userPhotoURL;
  String profitInvestment;
  String endInvestment;
  String startInvestment;

  Sond(
      {this.numberShares,
        this.balance,
        this.arrears,
        this.loan,
        this.remainingLoan,
        this.arrearsLoan,
        this.incorporationFee,
        this.filtering,
        this.waitingLoan,
        this.investor,
        this.amountInvestment,
        this.note,
        this.userId,
        this.userName,
        this.userPhotoURL,
        this.profitInvestment,
        this.endInvestment,
        this.startInvestment});

  Sond.fromJson(Map<String, dynamic> json) {
    numberShares = json['numberShares'];
    balance = json['balance'];
    arrears = json['arrears'];
    loan = json['loan'];
    remainingLoan = json['remainingLoan'];
    arrearsLoan = json['arrearsLoan'];
    incorporationFee = json['incorporationFee'];
    filtering = json['filtering'];
    waitingLoan = json['waitingLoan'];
    investor = json['Investor'];
    amountInvestment = json['amountInvestment'];
    note = json['note'];
    userId = json['userId'];
    userName = json['userName'];
    userPhotoURL = json['userPhotoURL'];
    profitInvestment = json['profitInvestment'];
    endInvestment = json['endInvestment'];
    startInvestment = json['startInvestment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['numberShares'] = this.numberShares;
    data['balance'] = this.balance;
    data['arrears'] = this.arrears;
    data['loan'] = this.loan;
    data['remainingLoan'] = this.remainingLoan;
    data['arrearsLoan'] = this.arrearsLoan;
    data['incorporationFee'] = this.incorporationFee;
    data['filtering'] = this.filtering;
    data['waitingLoan'] = this.waitingLoan;
    data['Investor'] = this.investor;
    data['amountInvestment'] = this.amountInvestment;
    data['note'] = this.note;
    data['userId'] = this.userId;
    data['userName'] = this.userName;
    data['userPhotoURL'] = this.userPhotoURL;
    data['profitInvestment'] = this.profitInvestment;
    data['endInvestment'] = this.endInvestment;
    data['startInvestment'] = this.startInvestment;
    return data;
  }
}
