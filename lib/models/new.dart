class New {
  int newsId;
  String title;
  String author;
  String newsDetail;

  New({this.newsId, this.title, this.author, this.newsDetail});

  New.fromJson(Map<String, dynamic> json) {
    newsId = json['newsId'];
    title = json['title'];
    author = json['author'];
    newsDetail = json['newsDetail'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['newsId'] = this.newsId;
    data['title'] = this.title;
    data['author'] = this.author;
    data['newsDetail'] = this.newsDetail;
    return data;
  }
}
