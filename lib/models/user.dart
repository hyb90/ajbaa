class User {
  int id;
  String name;
  String nationalId;
  String gender;
  String phoneNumber;
  String userPhotoPath;
  String createdAt;

  User(
      {this.id,
        this.name,
        this.nationalId,
        this.gender,
        this.phoneNumber,
        this.userPhotoPath,
        this.createdAt});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nationalId = json['nationalId'];
    gender = json['Gender'];
    phoneNumber = json['phoneNumber'];
    userPhotoPath = json['user_Photo_path'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['nationalId'] = this.nationalId;
    data['Gender'] = this.gender;
    data['phoneNumber'] = this.phoneNumber;
    data['user_Photo_path'] = this.userPhotoPath;
    data['created_at'] = this.createdAt;
    return data;
  }
}
