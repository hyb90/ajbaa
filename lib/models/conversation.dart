class Conversation {
  int senderId;
  String sender;
  String message;
  String createdAt;

  Conversation({this.senderId, this.sender, this.message, this.createdAt});

  Conversation.fromJson(Map<String, dynamic> json) {
    senderId = json['sender_id'];
    sender = json['Sender'];
    message = json['message'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['sender_id'] = this.senderId;
    data['Sender'] = this.sender;
    data['message'] = this.message;
    data['created_at'] = this.createdAt;
    return data;
  }
}
