class Photo {
  int photoId;
  String photoURL;


  Photo(
      {this.photoId,
        this.photoURL,
});

  Photo.fromJson(Map<String, dynamic> json) {
    photoId = json['photoId'];
    photoURL = json['photoURL'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['photoId'] = this.photoId;
    data['photoURL'] = this.photoURL;
    return data;
  }
}
